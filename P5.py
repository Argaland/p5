# Projet 5 groupe 4.7
from flask import Flask
from flask_ask import Ask, statement, question
app = Flask(__name__)
ask = Ask(app, '/')

#liste de recettes

# Dictionnaire contenant les recettes écrit sous forme :
# dic = {'recette1' : {'ingrédient1':prix1, 'ing2':p2, 'ing3':p3}, 'rec2' : {'i1':p1, 'i2':p2}}
recettes_dic = {
    'un moelleux au chocolat' : {'chocolat':5, 'beurre':3, 'sucre':3, 'farine':3, 'oeufs':4},
    'une omelette aux lardons' : {'oeufs':4, 'lardons':4},
    'un poulet, frites et salade' : {'poulet':5, 'pommes de terre':5, 'salade':3},
    'des crêpes' : {'du sucre':3,'du beurre':1,'de la farine':3,'des oeufs':2, 'du lait': 2},
    'un sandwich thon mayonnaise' : {'baguette':1, 'thon':3, 'mayonnaise':2, 'salade':2}
    'des pâtes carbonara' : {'pâtes' : 3, 'crème fraîche':2, 'oeufs':1, 'lardons':2, 'oignons':2}
    'des pâtes sauce tomate' : {'pâtes' : 3, 'tomates':2, 'oignons':2}
    'une pizza' : {'pâte à pizza': 3, 'sauce tomate': 3, 'fromage': 2, 'olives': 1}
    'du tiramisu' : {'boudoirs': 2, 'mascarpone': 2, 'café': 1, 'sucre': 3, 'oeufs': 2}
    'de la glace' : {'crème fraîche': 2, 'sirop': 3, 'sucre': 3}
}


model = {'un moelleux au chocolat':0, 'une omelette aux lardons':0, 'un poulet, frites et salade':0, 'des crêpes':0, 'un sandwich thon mayonnaise':0
         'des pâtes carbonara':0, 'des pâtes sauce tomate':0, 'une pizza':0, 'du tiramisu':0, 'de la glace':0}


def chercheur_recettes(liste):
    """
    pre: donner une liste (lst) d'ingédients (str)
    post: retourne le nom d'une recette avec un maximum de ces ingrédients
    """
    dic_resultats = model.copy()
    for ingr in liste:
        for rec in recettes_dic:
            if ingr in recettes_dic[rec].keys():
                dic_resultats[rec] += 1
    maxi = 0
    recette = None
    for rec in dic_resultats:
        if dic_resultats[rec] > maxi:
            maxi = dic_resultats[rec]
            recette = rec
    if recette == None:
        return "Je ne connais pas de recette avec un de vos ingrédients"
    
    ingr_manquants = []
    prix = 0
    for ingr in recettes_dic[recette]:
        if ingr not in liste:
            ingr_manquants.append(ingr)
            prix += recettes_dic[recette][ingr]
            
    if ingr_manquants != []:
        reponse = "Vous pouvez faire {}. Mais il vous manque : ".format(recette)
        for ingr in ingr_manquants:
            reponse += "{}, ".format(ingr)
        reponse = reponse[:-2] + ". "
        reponse += "Ces ingrédients devraient vous coûter {} euros.".format(prix)
        return reponse
    
    return "Vous pouvez faire {} !".format(recette)


#Fonction Alexa
@ask.launch
def start():
    return question("Bonjour, qu'est-ce qu'il te reste dans ton frigo ?")


@ask.intent('Recette')
def bonjour(allimentun = None, allimentdeux = None,allimenttrois = None ,allimentquatre = None):
    
    a1,a2,a3,a4 = allimentun,allimentdeux,allimenttrois,allimentquatre
    
    
    
    return statement(chercheur_recettes([a1,a2,a3,a4,a5]))


if __name__ == '__main__':
    app.run()